
const express = require('express');
const {MongoClient} = require('mongodb');
//pass rkNXzXdH83bGB2LS
const client = new MongoClient('mongodb+srv://bohdanchychukpz2022:rkNXzXdH83bGB2LS@cluster0.0rddcqz.mongodb.net/')
const path = require('path');
const mysql = require('mysql2/promise');
const app = express();
const morgan = require('morgan');
app.set('view engine', 'ejs');
const PORT = 3000;
const createPath = (page) => path.resolve(`C:\\Users\\Адмін\\pvi_lab_1\\public\\ejs-views\\${page}.ejs`);

//5 lab socket.io
const http = require('http').Server(app);

const io = require('socket.io')(http);

app.use(express.json());

//Whenever someone connects this gets executed
io.on('connection', function(socket) {
    console.log('A user connected');
 
    socket.on('userConnected', async ({ firstName, lastName }) => {
      try {
        await setStudentStatusOnline(firstName, lastName);

        console.log(`${firstName} ${lastName} is now online`);
    } catch (error) {
        console.error('Error updating status:', error);
    }

    });
    


    //Whenever someone disconnects this piece of code executed
    socket.on('disconnect', async () => {
      const user = socket.user; // збережене ім'я та прізвище користувача
      if (user) {
        await setStudentStatusOffline(user.firstName, user.lastName);
        console.log(`${user.firstName} ${user.lastName} is now offline`);
      }
    });
    socket.on('setUser', (user) => {
      socket.user = user; // збереження даних користувача
    });



    socket.on('new chat', (userIds) => {
    // Додаємо новий чат до списку чатів
    
    // Оновлюємо список чатів для всіх користувачів
    io.emit('update chat list', userIds);

  
  });


  socket.on('Send Massage', (dataArray,currentChatIdForMessage,senderId) => {
    // Додаємо новий чат до списку чатів
    
    // Оновлюємо список чатів для всіх користувачів
    
    io.emit('update massages', dataArray,currentChatIdForMessage,senderId);

  
  });


 });

//5 lab socket.io

//mongo
const startMongoDB =async()=>{
  try {
    await client.connect();
    console.log('MONGO:Connected to the database');
  }
    catch(error){
      console.log('MONGO:Error connecting to the database');
      console.log(error);
    }
  }
  startMongoDB();
  //
  //Реєстрація нового користувача
  //
  app.post('/add-User', async (req, res) => {
    try {
      let user = req.body;
      if (!user) {
        throw new Error('User object is undefined');
      }
  
      // Перевірка, чи користувач вже існує
      let existingUser = await client.db('PVI').collection('Users').findOne({ name: user.name, surname: user.surname });
      if (existingUser) {
        res.status(400).json({ message: "Користувач з таким ім'ям та прізвищем вже існує" });
        console.log("Користувач з таким ім'ям та прізвищем вже існує");
        return;
      }
  
      // Додавання нового користувача
      let result = await client.db('PVI').collection('Users').insertOne(user);
      res.status(200).json({ message: "User added successfully", userId: result.insertedId }); // Повертаємо userId
      console.log('User added successfully');
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Error adding user'});
    }
});

app.post('/update-User', async (req, res) => {
  try {
      let { OLDname, OLDsurname, name, surname, password } = req.body;

      if (!OLDname || !OLDsurname) {
          throw new Error('Старі дані користувача не вказані');
      }

      // Пошук користувача за старими даними
      let existingUser = await client.db('PVI').collection('Users').findOne({ name: OLDname, surname: OLDsurname });
      if (!existingUser) {
          res.status(404).json({ message: "Користувача зі старими даними не знайдено" });
          console.log("Користувача зі старими даними не знайдено");
          return;
      }

      // Оновлення даних користувача
      let result = await client.db('PVI').collection('Users').updateOne({ _id: existingUser._id }, { $set: { name, surname, password } });
      if (result.modifiedCount === 1) {
          res.status(200).json({ message: "Дані користувача оновлено успішно", userId: existingUser._id });
          console.log('Дані користувача оновлено успішно');
      } else {
          res.status(500).json({ message: "Помилка при оновленні даних користувача" });
          console.log('Помилка при оновленні даних користувача');
      }
  } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Помилка при оновленні користувача' });
  }
});

app.post('/delete-User', async (req, res) => {
  try {
      let { name, surname } = req.body;

      if (!name || !surname) {
          throw new Error('Не вказано ім\'я або прізвище користувача');
      }

      // Пошук користувача за ім'ям та прізвищем
      let existingUser = await client.db('PVI').collection('Users').findOne({ name, surname });
      if (!existingUser) {
          res.status(404).json({ message: "Користувача з вказаними ім'ям та прізвищем не знайдено" });
          console.log("Користувача з вказаними ім'ям та прізвищем не знайдено");
          return;
      }

      // Видалення користувача
      let result = await client.db('PVI').collection('Users').deleteOne({ _id: existingUser._id });
      if (result.deletedCount === 1) {
          res.status(200).json({ message: "Користувача видалено успішно" });
          console.log('Користувача видалено успішно');
      } else {
          res.status(500).json({ message: "Помилка при видаленні користувача" });
          console.log('Помилка при видаленні користувача');
      }
  } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Помилка при видаленні користувача' });
  }
});


  //
  //Вхід користувача
  //
 app.post('/login', async (req, res) => {
    try {
      let user = req.body;
      if (!user) {
        throw new Error('User object is undefined');
      }
  
      // Перевірка, чи користувач існує
      console.log(user);
      let existingUser = await client.db('PVI').collection('Users').findOne({ name: user.name, surname: user.surname });
      console.log(existingUser);
      if (!existingUser) {
        res.status(400).json({ message: "Користувач з таким ім'ям та прізвищем не існує" });
        console.log("Користувач з таким ім'ям та прізвищем не існує");
        return;
      }
      // Перевірка пароля
      else if (user.password !== existingUser.password) {
        res.status(400).json({ message: "Неправильний пароль" });
        console.log("Неправильний пароль");
        return;
      }
      else{
      res.status(200).json({ message: "Успішний вхід в систему", userId: existingUser._id }); // Повертаємо userId
      console.log('Успішний вхід в систему');
      }
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Error logging in user'});
    }
});

  

  // Маршрут для створення нового чату
app.post('/create-chat', async (req, res) => {
  try {
      let chat = req.body;
      if (!chat) {
          throw new Error('Chat object is undefined');
      }

      // Додавання нового чату
      let result = await client.db('PVI').collection('Chats').insertOne(chat);
      res.status(200).json({ message: "Chat created successfully", chatId: result.insertedId });
      console.log('Chat created successfully');
  } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Error creating chat'});
  }
});

// Маршрут для додавання користувача до чату
app.post('/add-user-to-chat', async (req, res) => {
  try {
      let { chatId, userId } = req.body;
      if (!chatId || !userId) {
          throw new Error('Chat ID or User ID is undefined');
      }

      // Додавання користувача до чату
      await client.db('PVI').collection('UserChats').insertOne({ chatId, userId });
      res.status(200).json({ message: "User added to chat successfully"});
      console.log('User added to chat successfully');
  } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Error adding user to chat'});
  }
});

//Cтворення повідомлення
const { ObjectId } = require('mongodb');

app.post('/create-message', async (req, res) => {
  try {
    let messageData = req.body;
    if (!messageData) {
      throw new Error('Message data is undefined');
    }

    // Додавання нового повідомлення
    let result = await client.db('PVI').collection('Messages').insertOne(messageData);
    res.status(200).json({ message: "Message created successfully", messageId: result.insertedId });
    console.log('Message created successfully');
  } catch (error) {
    console.log(error);
    res.status(500).json({ message: 'Error creating message'});
  }
});
//Кінець створення повідомлення
//----------------------------------------------------------------
//Знайти всі повідомлення 
app.get('/get-messages/:chatId', async (req, res) => {
  try {
    const chatId = req.params.chatId;
    console.log(chatId);
    // Запит на базу даних MongoDB для отримання всіх повідомлень з певного чату
    const messages = await client.db('PVI').collection('Messages').find({ "chatId": chatId }).toArray();

    res.status(200).json(messages);
  } catch (error) {
    console.error('Error fetching messages:', error);
    res.status(500).json({ message: 'Error fetching messages' });
  }
});
//Кінець знайти всі повідомлення

    //mongo 
    app.get('/get-Users', async (req, res) => {
      try {
        let users = await client.db('PVI').collection('Users').find().toArray();
        res.status(200).json(users);
      } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error getting users'});
      }
    });




    // Маршрут для отримання користувача за ім'ям
    app.post('/get-user-id', async (req, res) => {
      try {
          let { userName } = req.body;
          let [firstName, lastName] = userName.split(' ');
          let user = await client.db('PVI').collection('Users').findOne({ name: firstName, surname: lastName });
  
          if (user) {
              res.status(200).json(user._id);
          } else {
              res.status(404).json({ message: 'User not found'});
          }
      } catch (error) {
          console.log(error);
          res.status(500).json({ message: 'Error getting user ID'});
      }
  });

  app.post('/get-user-id-bycid', async (req, res) => {
    try {
        let { chatid } = req.body;
        
        let users = await client.db('PVI').collection('UserChats').find({ chatId: chatid}).toArray();
   
        if (users.length > 0) { // Перевірка наявності користувачів у чаті
            const userIds = users.map(user => user.userId); // Отримання масиву ідентифікаторів користувачів
            res.status(200).json(userIds);
        } else {
            res.status(404).json({ message: 'No users found for the chat'});
        }
    } catch (error) {
        console.error('Error getting user IDs by chat ID:', error);
        res.status(500).json({ message: 'Error getting user IDs by chat ID'});
    }
});


  app.post('/get-user-name', async (req, res) => {
    try {
        const { userId } = req.body;
        const objectId = new ObjectId(userId);
        const user = await client.db('PVI').collection('Users').findOne({ _id: objectId  });
        if (user) {
            const fullName = user.name + " " + user.surname;
            res.status(200).json(fullName);
        } else {
            res.status(404).json({ message: 'User not found' });
        }
    } catch (error) {
        console.error('Error getting user name:', error);
        res.status(500).json({ message: 'Error getting user name' });
    }
});

  
  //----------------------------------------------------------------
  //отримання чатів з бд

  //id чатів
  app.post('/get-user-chats', async (req, res) => {
    try {
      // Отримати ID користувача з запиту
      let currentUserId = req.body.currentUserId;
  
      // Отримати всі чати користувача з бази даних
      let userChats = await client.db('PVI').collection('UserChats').find({userId: currentUserId}).toArray();
  
      // Витягнути ID чатів
      let chatIds = userChats.map(chat => chat.chatId);
  
      console.log(chatIds);
      res.status(200).json(chatIds);
    } catch (error) {
      console.error('Помилка при отриманні чатів:', error);
      res.status(500).json({ message: 'Помилка при отриманні чатів' });
    }
  });
  
  //----------------------------------------------------------------
  //Ід юзерів в чаті
  //id користувачів
  

app.post('/get-chat-users', async (req, res) => {
  const { ObjectId } = require('mongodb');
  try {
    // Отримати ID чату з запиту
    let currentChatId = req.body.currentChatId;

    // Отримати всі записи користувачів з бази даних, які мають цей ID чату
    let chatUsers = await client.db('PVI').collection('UserChats').find({chatId: currentChatId}).toArray();

    // Витягнути ID користувачів
    let userIds = chatUsers.map(userChat => new ObjectId(userChat.userId));

    // Отримати імена користувачів за їх ID
    let users = await client.db('PVI').collection('Users').find({_id: {$in: userIds}}).toArray();
    let userNames = users.map(user => `${user.name} ${user.surname}`);

    console.log(userNames);
    res.status(200).json(userNames);
  } catch (error) {
    console.error('Помилка при отриманні користувачів чату:', error);
    res.status(500).json({ message: 'Помилка при отриманні користувачів чату' });
  }
});


app.post('/get-chat-users', async (req, res) => {
  try {
      const { chatId } = req.body;
      const userChats = await client.db('PVI').collection('UserChats').find({ chatId }).toArray();
      
      if (userChats.length > 0) {
          const userIds = userChats.map(userChat => userChat.userId);
          console.log(userIds);
          res.status(200).json(userIds);
      } else {
          res.status(404).json({ message: 'No users found for the chat' });
      }
  } catch (error) {
      console.error('Error getting chat users:', error);
      res.status(500).json({ message: 'Error getting chat users' });
  }
});

  

  //----------------------------------------------------------------
  // Маршрут для отримання списку чатів
  app.post('/get-chats', async (req, res) => {
    try {
      // Отримати ID чатів з запиту
      const { ObjectId } = require('mongodb');

let chatIds = req.body.chatIds;
console.log('chatIDs:');
console.log(chatIds);

// Перетворити рядки в ObjectId
let objectIds = chatIds.map(id => new ObjectId(id));

// Отримати чати з бази даних
const chats = await client.db('PVI').collection('Chats').find({ _id: { $in: objectIds } }).toArray();

      
      console.log('/get-chats:chats:');
      console.log(chats);
      res.status(200).json(chats);
    } catch (error) {
      console.error('Помилка при отриманні чатів:', error);
      res.status(500).json({ message: 'Помилка при отриманні чатів' });
    }
  });
  

  //----------------------------------------------------------------

//mongo 


http.listen(PORT, (error) => {
    error ? console.log(error) : console.log(`listening port ${PORT}`);
  });

app.use(express.static('public'));  
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));
app.use(express.urlencoded({ extended: false }));
//CHYCHUK-BOHDAN\SQLEXPRESS (CHYCHUK-BOHDAN\Адмін)
const config = require('./config');
async function Select_All_From_DB() {
  const conn = await mysql.createConnection(config);
  const [rows,fields] = await conn.execute('SELECT * FROM Students');
  console.log(rows);
  conn.end();
  
}

// Збереження студента в базі даних
async function Add_Student_To_DB(student,res) {
  try {
    const conn = await mysql.createConnection(config);
    await conn.execute(
      'INSERT INTO Students (id, `group`, firstName, lastName, gender, birthday) VALUES (?, ?, ?, ?, ?, ?)',
      [student.id, student.group, student.firstName, student.lastName, student.gender, student.birthday]
    );
    conn.end();
  } catch (error) {
    res.json({ message: `Помилка при додаванні на стороні бази данних : ${error}`  });
    console.error(`Помилка при збереженні студента: ${error}`);
  }
}




async function setStudentStatusOnline(firstName, lastName) {
  try {
    const conn = await mysql.createConnection(config);
    await conn.execute(
      'UPDATE Students SET status = ? WHERE firstName = ? AND lastName = ?',
      [true, firstName, lastName]
    );
    conn.end();
  } catch (error) {
    console.error(`Error setting student status online: ${error}`);
  }
}


async function setStudentStatusOffline(firstName, lastName) {
  try {
    const conn = await mysql.createConnection(config);
    await conn.execute(
      'UPDATE Students SET status = ? WHERE firstName = ? AND lastName = ?',
      [false, firstName, lastName]
    );
    conn.end();
  } catch (error) {
    console.error(`Error setting student status offline: ${error}`);
  }
}

















// Редагування студента в базі Данних 
async function Edit_Student_In_DB(student,res) {
  try {
    const conn = await mysql.createConnection(config);
    await conn.execute(
      'UPDATE Students SET `group` = ?, firstName = ?, lastName = ?, gender = ?, birthday = ? WHERE id = ?',
      [student.group, student.firstName, student.lastName, student.gender, student.birthday, student.id]
    );
    conn.end();
  } catch (error) {
    res.json({ message: `Помилка при редагуванні на стороні бази данних : ${error}`  });
    console.error(`Помилка при редагуванні студента: ${error}`);
    return;
  }
}


// Завантаження всіх студентів з бази даних
async function Get_All_Students_From_DB() {
  try {
    const conn = await mysql.createConnection(config);
    const [rows, fields] = await conn.execute('SELECT * FROM Students');
    conn.end();
    return rows;
  } catch (error) {
    console.error(`Помилка при завантаженні студентів: ${error}`);
  }
}


app.get('/get-students', async (req, res) => {
  try {
    students = await Get_All_Students_From_DB(res);
    res.json(students);
  } catch(error) {
    console.error('Помилка:', error);
  }
});

/*
connection.connect((error) => {
  error? console.log(error) : console.log('Connected to the database');
});
*/
let students = []; // Це масив для зберігання студентів на сервері
app.use(express.json());

app.post('/add-student', (req, res) => {
  try {
  // Отримання даних студента з запиту
  let student = req.body;

  // Перевірка даних
  if (!student.id || !student.group || !student.firstName || !student.lastName || !student.gender || !student.birthday) {
    res.json({ error: "Не всі поля заповнено"  });
    return;
  }
   // Перевірка за допомогою регулярних виразів
   let nameRegex = /^[A-ZА-Я][a-zа-я]{1,30}$/; // Перша буква велика, всі інші маленькі, без символів, максимальна довжина 30
   let cyrillicRegex = /[А-Яа-яЁё]/; // Перевірка на наявність кирилиці
   let latinRegex = /[A-Za-z]/; // Перевірка на наявність латиниці
 
   if (!nameRegex.test(student.firstName) || !nameRegex.test(student.lastName) || 
       (cyrillicRegex.test(student.firstName) !== cyrillicRegex.test(student.lastName)) || 
       (latinRegex.test(student.firstName) !== latinRegex.test(student.lastName))) {
     alert("Ім'я та прізвище повинні починатися з великої літери, містити лише букви, не містити символів, мати максимальну довжину 30 символів та бути написаними на одній мові");
     return;
   }

  // Перевірка віку студента
  let today = new Date();
  let birthDate = new Date(student.birthday);
  let age = today.getFullYear() - birthDate.getFullYear();
  let m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  if (age < 17|| age > 70) {
    alert("Студену має бути  17-70 років.  ");
    return;
  }

  // Збереження студента
  students.push(student);

  // Збереження студента в базі даних
  Add_Student_To_DB(student,res) ;

  // Відправка відповіді назад до клієнта
  res.json({ message: "Студента успішно збережено" });
  }catch(error) {
    console.error('Помилка:', error);
  }
});




app.put('/edit-student', (req, res) => {
  try{
    // Отримання даних студента з запиту
    let student = req.body;
  
    // Перевірка даних
    if (!student.id || !student.group || !student.firstName || !student.lastName || !student.gender || !student.birthday) {
      res.json({ error: "Не всі поля заповнено" });
      return;
    }

     // Перевірка за допомогою регулярних виразів
   let nameRegex = /^[A-ZА-Я][a-zа-я]{1,30}$/; // Перша буква велика, всі інші маленькі, без символів, максимальна довжина 30
   let cyrillicRegex = /[А-Яа-яЁё]/; // Перевірка на наявність кирилиці
   let latinRegex = /[A-Za-z]/; // Перевірка на наявність латиниці
 
   if (!nameRegex.test(student.firstName) || !nameRegex.test(student.lastName) || 
       (cyrillicRegex.test(student.firstName) !== cyrillicRegex.test(student.lastName)) || 
       (latinRegex.test(student.firstName) !== latinRegex.test(student.lastName))) {
     alert("Ім'я та прізвище повинні починатися з великої літери, містити лише букви, не містити символів, мати максимальну довжину 30 символів та бути написаними на одній мові");
     return;
   }
  // Перевірка віку студента
  let today = new Date();
  let birthDate = new Date(student.birthday);
  let age = today.getFullYear() - birthDate.getFullYear();
  let m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  if (age < 17|| age > 70) {
    alert("Студену має бути  17-70 років.  ");
    return;
  }
  
    // Знаходження студента для оновлення
    let index = students.findIndex(s => s.id === student.id);
    if (index === -1) {
      res.json({ error: "Студента не знайдено" });
      return;
    }
  
    // Оновлення даних студента
    students[index] = student;
    Edit_Student_In_DB(student,res);
    // Відправка відповіді назад до клієнта
    res.json({ message: "Дані студента успішно оновлено оновлені данні"  });
  }catch(error) {
    console.error('Помилка:', error);
    return
  }
  });






  async function Delete_Student_From_DB(id, res) {
    try {
      const conn = await mysql.createConnection(config);
      const [rows] = await conn.execute(
        'DELETE FROM Students WHERE id = ?',
        [id]
      );
      conn.end();
      if (rows.affectedRows === 0) {
        res.json({ error: "Студента не знайдено в базі данних ,оновіть сторінку щоб мати актуальну таблицю " });
        return false;
      }
      return true;
    } catch (error) {
      res.json({ message: `Помилка при видаленні на стороні бази данних : ${error}`  });
      console.error(`Помилка при видаленні студента: ${error}`);
      return false;
    }
  }
  




  app.delete('/delete-student', async (req, res) => {
    try{
      // Отримання id студента з запиту
      let id = req.body.id;
  
      // Знаходження студента для видалення
      let index = students.findIndex(s => s.id === id);
      if (index === -1) {
        res.json({ error: "Студента не знайдено" });
        return;
      }
  
      // Видалення студента
      students.splice(index, 1);
      const result = await Delete_Student_From_DB(id, res);
  
      // Відправка відповіді назад до клієнта
      if (result) {
        res.json({ message: "Студента успішно видалено" });
      }
    }catch(error) {
      console.error('Помилка:', error);
    }
  });
  
  

app.get('/', (req, res) => {
    res.redirect('/reg');
});

app.get('/reg' ,(req, res)=>  {
  res.render(createPath('Register'));
    
});

app.get('/login' ,(req, res)=>  {
  res.render(createPath('login'));
    
});

app.get('/students' , (req, res)=>  {
  res.render(createPath('index'));
    
});

app.get('/messages' , (req, res)=>  {
res.render(createPath('Messages'));

});


 


app.use((req, res) => {
   
    res
    
    .render(createPath('error'));
});