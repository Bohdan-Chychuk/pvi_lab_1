// JavaScript
var modal = document.getElementById("myModal");
var btn = document.getElementById("myBtn");
var span = document.getElementsByClassName("close")[0];
var EditORAdd =null
var isLogIn = false;
let globalfirstName = "";
let globallastName = "";
document.addEventListener('DOMContentLoaded', (event) => {
  // Функція для отримання значення cookie за ім'ям
  function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  }

  // Отримання значення cookie
  let firstName = getCookie('name');
  globalfirstName = firstName;
  let lastName = getCookie('surname');
  globallastName = lastName;
  if (firstName==undefined || lastName==undefined) {
    isLogIn = false;
    document.querySelector('.UserName').style.display = "none";
    document.getElementById("ProfileSpan").style.display = "none";
    document.getElementById("BellSpan").style.display = "none";
    
    document.getElementById("logoutButton").style.display = "block";
    
  }
  else {
    document.getElementById("loginButton").style.display = "none";
    document.querySelector('.UserName').style.display = "block";
    isLogIn = true;

    const socket = io();

 

    // Відправлення даних користувача при підключенні
    socket.emit('userConnected', { firstName, lastName });
  
    // Збереження даних користувача у сокеті
    socket.emit('setUser', { firstName, lastName });
   
    updateTable();

  }

 

  // Встановлення імені користувача
  document.querySelector('.UserName').textContent = firstName + ' ' + lastName;
});

document.getElementById("loginButton").addEventListener("click", function() {
  // Перенаправлення на сторінку входу
  window.location.href = "/login";
});

btn.onclick = function() {
  EditORAdd="Add";
  document.getElementById("modalTitle").textContent= "Додавання студента";
  document.getElementById("actionBtn").textContent= "Додати";
  modal.style.display = "block";
}

span.onclick = function() {
  modal.style.display = "none";
}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  
  }
}

let indexfordelete
//Модальне видалити студента
var modalDelStudent=document.getElementById("myModalDeleteStudent");
var delStudentButtons = document.getElementsByClassName("DelStudent");
for (let i = 0; i < delStudentButtons.length; i++) {
  delStudentButtons[i].onclick = function() {
    // Get the message element
    var message = document.getElementById("MassageDelStud");
   
    // Set the message text
    indexfordelete = (currentPage * 4) + i ;
    var name = students[indexfordelete].firstName + " " + students[indexfordelete].lastName;
    message.innerText = "Ви дійсно хочете видалити користувача " + name + "?";
    modalDelStudent.style.display = "block";
  };
  
}
document.getElementById("DeketeBtn").addEventListener('click', function() {
  // Отримання id студента, який потрібно видалити
  let id = students[indexfordelete].id;

  let userToDelete = {
    name: students[indexfordelete].firstName ,
    surname: students[indexfordelete].lastName
};

// Викликаємо API для видалення користувача
fetch('/delete-User', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(userToDelete)
})
.then(response => {
    if (response.ok) {
        return response.json();
    } else {
        throw new Error('Помилка ' + response.status);
    }
})
.then(data => {
    console.log(data); // Отримуємо повідомлення про успішне видалення або помилку
    // Ваш код для обробки успішної відповіді
})
.catch(error => {
    console.error(error); // Виводимо будь-яку помилку в консоль
    return ;
    // Ваш код для обробки помилки
});


  
  // Відправка DELETE запиту до сервера з id студента
  fetch('/delete-student', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ id }),
  })
  .then(response => response.json())
  .then(data => {
    if (data.error) {
      alert("Помилка: " + data.error);
    } else {
      if (students[indexfordelete].firstName ==globalfirstName && students[indexfordelete].lastName==globallastName) { 
        function deleteCookie(name) {
          document.cookie = name + "=; Max-Age=-99999999;";
      }
      
      // Використання функції для очищення кукі
      deleteCookie('name');
      deleteCookie('surname');
      deleteCookie('userId');

      window.location.reload();
    }
      // Видалення студента з масиву студентів
      students.splice(indexfordelete, 1);

      // Оновлення таблиці
      updateTable();
      modalDelStudent.style.display = "none";
      alert("Студента успішно видалено");

    }
  })
  .catch((error) => {
    console.error('Помилка:', error);
  });
});


document.getElementById("DelCancelBtn").addEventListener('click',function(){
  modalDelStudent.style.display ="none" ;
});



window.onclick = function(event) {
  if (event.target == modalDelStudent) {
    modalDelStudent.style.display = "none";
  
  }
}

var spanDelstud = document.getElementsByClassName("close")[1];
spanDelstud.onclick = function() {
  modalDelStudent.style.display = "none";
}


//
//Редагування данних про студента 
//
let OLDfirstName = "" ;
let OLDlastName = ""  ;

var btnEdit = document.getElementsByClassName("EditStudent");
let indexforEdit
for (let i = 0; i < btnEdit.length; i++) {
  btnEdit[i].onclick = function() {
  indexforEdit= (currentPage * 4) + i ;
  EditORAdd="Edit";
  document.getElementById("modalTitle").textContent= "Редагування студента";
  document.getElementById("actionBtn").textContent= "Редагувати";
  let student=students[indexforEdit];

  document.getElementById('Group').value = student.group;
  document.getElementById('fname').value = student.firstName;
  document.getElementById('lname').value = student.lastName;
  document.getElementById('Gender').value = student.gender;
  document.getElementById('birthday').value = student.birthday;

  OLDfirstName = document.getElementById('fname').value;
  OLDlastName = document.getElementById('lname').value;

  modal.style.display = "block";
  };
}

async function editStudent() {
  // Отримання значень форми
  let id = students[indexforEdit].id;
  let group = document.getElementById('Group').value;
  let firstName = document.getElementById('fname').value;
  let lastName = document.getElementById('lname').value;
  let gender = document.getElementById('Gender').value;
  let birthday = document.getElementById('birthday').value;

     // Перевірка за допомогою регулярних виразів
     let nameRegex = /^[A-ZА-Я][a-zа-я]{2,30}$/; // Перша буква велика, всі інші маленькі, без символів, максимальна довжина 30
     let cyrillicRegex = /[А-Яа-яЁё]/; // Перевірка на наявність кирилиці
     let latinRegex = /[A-Za-z]/; // Перевірка на наявність латиниці
   
     if (!nameRegex.test(firstName) || !nameRegex.test(lastName) || 
         (cyrillicRegex.test(firstName) !== cyrillicRegex.test(lastName)) || 
         (latinRegex.test(firstName) !== latinRegex.test(lastName))) {
       alert("Ім'я та прізвище повинні починатися з великої літери, містити лише букви, не містити символів, мати довжину 2-30 символів та бути написаними на одній мові");
       return;
     }
  
    // Перевірка віку студента
    let today = new Date();
    let birthDate = new Date(birthday);
    let age = today.getFullYear() - birthDate.getFullYear();
    let m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
      age--;
    }
    if (age < 17|| age > 70) {
      alert("Студену має бути  17-70 років.  ");
      return;
    }
  

  // Створення об'єкта студента для оновлення
  let student = {id, group, firstName, lastName, gender, birthday};





  try {
    let response = await fetch('/update-User', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        OLDname : OLDfirstName,
        OLDsurname :OLDlastName,
        name: firstName,
        surname: lastName,
        password: "1"
      })
    });
    if(response.status === 400)
    {
      alert('Не вдалося змінити дані користувача ,введіть інші дані');
      return ;
    }
    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    } else {
      //let user = await response.json(); // Отримання об'єкта користувача від сервера
     // document.cookie = `name=${firstName}; path=/`;
      //document.cookie = `surname=${lastName}; path=/`;
      //document.cookie = `userId=${user.userId}; path=/`; // Зберігання userId в куках
      alert('Користувач успішно Оновлений');
    //  window.location.href = "/students"; // Перехід на наступну сторінку
    }
  } catch (error) {
    console.error('Помилка:', error);
    alert('Помилка при оновленні');
    return ;
  }

  // Відправка PUT запиту до сервера з даними студента
  fetch('/edit-student', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(student),
  })
  .then(response => response.json())
  .then(data => {
    if (data.error) {
      alert("Помилка: " + data.error);
    } else {
      // Оновлення даних студента в масиві студентів
      students[indexforEdit] = student;

      // Оновлення таблиці
      updateTable();
      alert("Дані оновлено");
    }
  })
  .catch((error) => {
    console.error('Помилка:', error);
  });
}





// Масив студентів
let students = [];
let studentId = 1;

document.addEventListener('DOMContentLoaded', async function() {
  
  setTimeout(async function() {
    const response = await fetch('/get-students');
    students = await response.json();
    //alert(students[students.length - 1].id);
    studentId = (students[students.length - 1].id) + 1;

    updateTable();
  }, 1000);
});
// Функція додавання студента
 // Це глобальний ідентифікатор, який збільшується кожного разу, коли додається новий студент

 async function addStudent() {
  // Отримання значень форми
  
  let group = document.getElementById('Group').value;
  let firstName = document.getElementById('fname').value;
  let lastName = document.getElementById('lname').value;
  let gender = document.getElementById('Gender').value;
  let birthday = document.getElementById('birthday').value;

  // Перевірка чи всі поля заповнені
  if (!group || !firstName || !lastName || !gender || !birthday) {
    alert("Не всі поля заповнено");
    return;
  }

   // Перевірка за допомогою регулярних виразів
   let nameRegex = /^[A-ZА-Я][a-zа-я]{2,30}$/; // Перша буква велика, всі інші маленькі, без символів, максимальна довжина 30
   let cyrillicRegex = /[А-Яа-яЁё]/; // Перевірка на наявність кирилиці
   let latinRegex = /[A-Za-z]/; // Перевірка на наявність латиниці
 
   if (!nameRegex.test(firstName) || !nameRegex.test(lastName) || 
       (cyrillicRegex.test(firstName) !== cyrillicRegex.test(lastName)) || 
       (latinRegex.test(firstName) !== latinRegex.test(lastName))) {
     alert("Ім'я та прізвище повинні починатися з великої літери, містити лише букви, не містити символів, мати довжину 2-30 символів та бути написаними на одній мові");
     return;
   }

  // Перевірка віку студента
  let today = new Date();
  let birthDate = new Date(birthday);
  let age = today.getFullYear() - birthDate.getFullYear();
  let m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
    age--;
  }
  if (age < 17|| age > 70) {
    alert("Студену має бути  17-70 років.  ");
    return;
  }



  // Створення нового студента з унікальним id
  let student = {id: studentId++, group, firstName, lastName, gender, birthday};

  // Відправка даних на сервер
    try {
      let response = await fetch('/add-User', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          name: firstName,
          surname: lastName,
          password: "1"
        })
      });
      if(response.status === 400)
      {
        alert('Такий користувач вже існує ,введіть інші дані');
        return ;
      }
      if (!response.ok) {
        throw new Error(`HTTP error! status: ${response.status}`);
      } else {
        alert('Користувач успішно зареєстрований він може ввійти (Пароль 1)');
        window.location.href = "/students"; // Перехід на наступну сторінку
      }
    } catch (error) {
      console.error('Помилка:', error);
      alert('Помилка при реєстрації користувача');
      return ;
    }
  // Відправка POST запиту до сервера з даними студента
  fetch('/add-student', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(student),
  })
  .then(response => response.json())
  .then(data => {
    if (data.error) {
      alert("Помилка: " + data.error);
    } else {
      // Додавання студента до масиву студентів
      students.push(student);

      // Оновлення таблиці
      updateTable();
    }
  })
  .catch((error) => {
    console.error('Помилка:', error);
  });
}



// Поточна сторінка
let currentPage = 0;

// Функція оновлення таблиці
function updateTable() {
  
  // Отримання таблиці
  let table = document.querySelector('.Table table');

  // Очищення таблиці
  for (let i = 1; i < table.rows.length; i++) {
    let row = table.rows[i];
    row.cells[1].innerText = '';
    row.cells[2].innerText = '';
    row.cells[3].innerText = '';
    row.cells[4].innerText = '';
    row.cells[5].innerText = '';
    row.querySelector('.buttonin' + i + 'row').style.visibility = 'hidden';
  }

  // Додавання студентів до таблиці
  for (let i = 0; i < 4; i++) {
    let index = currentPage * 4 + i;
    if (index < students.length) {
      let row = table.rows[i + 1];
      row.cells[1].innerText = students[index].group;
      row.cells[2].innerText = students[index].firstName +" "+ students[index].lastName ;
      row.cells[3].innerText = students[index].gender;
      row.cells[4].innerText = students[index].birthday;

      
      
      
      // Створення нового елемента div для  крапки
      let dot = document.createElement('div');
      
      if(students[index].status ==false)dot.style.backgroundColor = 'red';
      else dot.style.backgroundColor = 'green';
      dot.style.borderRadius = '50%';
      dot.style.width = '20px';  // Збільшуємо розмір крапки
      dot.style.height = '20px'; // Збільшуємо розмір крапки
      dot.style.display = 'inline-block';
      dot.style.margin = '0 auto'; // Вирівнюємо крапку по центру

      // Додаємо зелену крапку в 5-й стовпець
      let cell = row.cells[5];
      cell.innerHTML = '';
      cell.style.textAlign = 'center'; // Вирівнюємо контент клітинки по центру
      cell.appendChild(dot);


      row.querySelector('.buttonin' + (i + 1) + 'row').style.visibility = 'visible';
    }
  }
}



// Функція перемикання сторінки
function changePage(event) {
  let button = event.target;
  if (button.innerText === '<') {
    if (currentPage > 0) {
      currentPage--;
    }
  } else if (button.innerText === '>') {
    if (currentPage < Math.ceil(students.length / 4) - 1) {
      currentPage++;
    }
  } else {
    let pageNumber = parseInt(button.innerText);
    if (!isNaN(pageNumber)) {
      currentPage = pageNumber - 1;
    }
  }
  updateTable();
}

// Приєднання події до кнопок навігації
let buttons = document.querySelectorAll('.buttons-change-table-page button');
for (let i = 0; i < buttons.length; i++) {
  buttons[i].addEventListener('click', changePage);
}

// Приєднання події до кнопки "Створити"
document.getElementById('actionBtn').addEventListener('click', function() {
  if(EditORAdd=="Edit")
  editStudent();
  else
  addStudent();
});

//Анімація дзвіночка 

let bell = document.getElementById('BellSpan');
let newIcon = document.createElement('img');
newIcon.src = "assets/Notification_Bell_SVG_red_Dot.svg";
newIcon.style.position = "absolute";
newIcon.style.height = "40px";
newIcon.style.width = "40px";
newIcon.style.display = "none"; 
newIcon.style.top="0px";

bell.appendChild(newIcon);

// setInterval(function() {
//     if (newIcon.style.display === "none") {
//         newIcon.style.display = "block"; // Show 
//     } else {
//         newIcon.style.display = "none"; // Hide 
//     }
// }, 1000); //5 sec


//Функція додавання студента 
document.getElementById("GET").addEventListener('click', function() {
  // Відправка GET запиту до сервера
  fetch('/get-students')
  .then(response => response.json())
  .then(data => {
    if (data.error) {
      alert("Помилка: " + data.error);
    } else {
      // Оновлення масиву студентів
      students = data.students;

      // Оновлення таблиці
      updateTable();
      alert("Дані успішно отримано");
    }
  })
  .catch((error) => {
    console.error('Помилка:', error);
  });
});



////////////////////////////////
//
//5 Лаба ///////////////////////
//
////////////////////////////////

document.getElementById('BellSpan').addEventListener('click', function() {
  window.location.href = 'http://localhost:3000/messages'; 
});


