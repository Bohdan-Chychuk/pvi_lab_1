// Функція для створення пульсуючого червоного круга
function createPulseCircle() {
    let pulseCircle = document.createElement('div');
    pulseCircle.style.position = "right:5px";
    pulseCircle.style.width = "10px";
    pulseCircle.style.height = "10px";
    pulseCircle.style.borderRadius = "50%";
    pulseCircle.style.backgroundColor = "red";
    pulseCircle.style.opacity = "0.5";
    pulseCircle.style.animation = "pulse 1s infinite alternate";

    return pulseCircle;
}

// Додаємо пульсуючий круг до HederBell і повертаємо його для подальшого використання
function startPulseAnimation() {
    let hederBell = document.getElementById('HederBell');
    let pulseCircle = createPulseCircle();
    hederBell.appendChild(pulseCircle);
    return pulseCircle;
}

// Функція для завершення анімації
function stopPulseAnimation(pulseCircle) {
    if (pulseCircle && pulseCircle.parentNode) {
        pulseCircle.parentNode.removeChild(pulseCircle);
    }
}

// Створюємо стилі для анімації
let style = document.createElement('style');
style.innerHTML = `
@keyframes pulse {
    0% {
        transform: scale(1);
        opacity: 0.5;
    }
    100% {
        transform: scale(1.5);
        opacity: 0;
    }
}
`;

// Додаємо стилі до сторінки
document.head.appendChild(style);

// Початок анімації
let pulseCircle = null;






currentUserId = "";
//Глобальний об'єкт для зберігання повідомлень
const messageStore = {
    messages: []
};

document.addEventListener('DOMContentLoaded', (event) => {
    function getCookie(name) {
        let matches = document.cookie.match(new RegExp(
          "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
      }

    currentUserId = getCookie('userId');


});

async function getUserName(userId) {
    try {
        const response = await fetch('/get-user-name', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ userId })
        });

        const fullName = await response.json();
        return fullName;
    } catch (error) {
        console.error('Error getting user name:', error);
        throw new Error('Error getting user name');
    }
}

async function getMessages(chatId) {
    try {
      const response = await fetch(`/get-messages/${chatId}`);
      const messages = await response.json();
      return messages;
    } catch (error) {
      console.error('Error fetching messages:', error);
      throw new Error('Error fetching messages');
    }
  }



  function createMessageElementNotifications(message, isSender) {
    // Створюємо новий div для повідомлення
    const messageDiv = document.createElement('div');
    messageDiv.className = isSender ?  'message receiver': 'message sender';
  
    // Додамо вміст повідомлення
    const textDiv = document.createElement('div');
    textDiv.className = 'text';
    const strong = document.createElement('strong');
   
    getUserName(message.userId)
    .then(fullName => {
        console.log('User full name:', fullName);
        strong.textContent=fullName;
        // Далі ви можете використати fullName для подальшої обробки
    })
    .catch(error => {
        console.error('Error:', error);
    });

   
    const p = document.createElement('p');
    p.textContent = message.message; // Текст повідомлення
    textDiv.appendChild(strong);
    textDiv.appendChild(p);
    messageDiv.appendChild(textDiv);
  
    // Додамо час відправлення
    const timestamp = document.createElement('p');
    timestamp.textContent = new Date(message.timestamp).toLocaleString(); // Припустимо, що timestamp містить дату та час відправлення
    textDiv.appendChild(timestamp);
  
    // Додаємо повідомлення до контейнера чату
    const chatContainer = document.getElementById('chatContainer');
    document.getElementById("BellMouseHoverText").appendChild(messageDiv);
  }



















document.getElementById('BellSpan').addEventListener('mouseover', function() {
    stopPulseAnimation(pulseCircle); 
    document.getElementById('BellMouseHoverText').style.display = 'block';
});

document.getElementById('BellSpan').addEventListener('mouseout', function() {
    document.getElementById('BellMouseHoverText').style.display = 'none';
});

socket.on('update massages', (dataArray, currentChatIdForMessage2, senderId) => {
if(currentUserId==senderId){
    
}
else{
getMessages(currentChatIdForMessage2)
.then(messages => {
    // Визначення кількості нових повідомлень
    const newMessagesCount = messages.length > 3 ? messages.length - 3 : 0;
    
    document.getElementById("BellMouseHoverText").innerHTML = "";
    // Відображення максимум 3 останніх повідомлень
    for (let i = Math.max(messages.length - 3, 0); i < messages.length; i++) {
        const message = messages[i];
        if (message.userId !== currentUserId) { // Перевірка, чи не є користувач автором повідомлення
            const isSender = false; // Встановлюємо значення isSender в false
            createMessageElementNotifications(message, isSender);
        }
    }
    pulseCircle = startPulseAnimation();


})
.catch(error => {
    // Обробка помилки
    console.error('Error fetching messages:', error);
});
}
});


