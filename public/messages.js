let span2 = document.getElementsByClassName("close")[0];
let currentChatIdForMessage ="";
userIdsForMessage = [];
let isCreatingChat = false;
let isCreatingMessage = false;
let membersname="";
let curentusername=null;
let currentUserId="";
let selectedUsers = []; // Масив для зберігання вибраних користувачів
let existingDialogs = []; // Масив для зберігання існуючих діалогів
let existingGroups = {}; // Об'єкт для зберігання існуючих груп та їх учасників
document.addEventListener('DOMContentLoaded', (event) => {
    // Функція для отримання значення cookie за ім'ям
    function getCookie(name) {
      let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
      ));
      return matches ? decodeURIComponent(matches[1]) : undefined;
    }
  
    // Отримання значення cookie
    let firstName = getCookie('name');
    let lastName = getCookie('surname');
    currentUserId = getCookie('userId');
    // Встановлення імені користувача
    curentusername= firstName + ' ' + lastName;
    displayChats();
    // Встановлення імені користувача
    document.querySelector('.UserName').textContent = firstName + ' ' + lastName;
    //alert(curentusername);
        // Відправлення даних користувача при підключенні
        socket.emit('userConnected', { firstName, lastName });
  
        // Збереження даних користувача у сокеті
        socket.emit('setUser', { firstName, lastName });
  });
  
// Коли користувач натискає на <span> (x), закрити модальне вікно
span2.onclick = function() {
  let modal = document.getElementById('myModal');
  modal.style.display = "none";
  selectedUsers = [];
}
document.getElementById('showMembers').addEventListener('click',async function() {
let membersnamelist =document.getElementById('chatMembers');

if (membersnamelist.style.display === 'block') {
    membersnamelist.style.display = 'none';
   
}
else{
membersnamelist.style.display = "block";
membersnamelist.textContent = membersname;
}
});


document.getElementById("newchatroombutton").addEventListener("click", async function(event){

    // Отримання списку користувачів з сервера
    let response = await fetch('/get-Users');
    let users = await response.json();
  
   // Отримання слайдера та очищення його
  let slider = document.getElementById('slider');
  slider.innerHTML = '';

  // Додавання користувачів до слайдера
// Створюємо контейнер для користувачів
let userContainer = document.createElement('div');
userContainer.style.height = '300px'; // Фіксований розмір
userContainer.style.overflow = 'auto'; // Дозволяє прокрутку



users.forEach(user => {
    if((user.name + ' ' + user.surname)!==curentusername ){
    let userDiv = document.createElement('div');
    userDiv.textContent = user.name + ' ' + user.surname;

    // Додаємо зелену обводку, заокруглені краї і відстань
    userDiv.style.border = '1px solid green';
    userDiv.style.borderRadius = '10px';
    userDiv.style.margin = '10px';

    // Змінюємо курсор на палець при наведенні
    userDiv.style.cursor = 'pointer';

    // Робимо текст по середині і збільшуємо розмір
    userDiv.style.textAlign = 'center';
    userDiv.style.padding = '20px';
    userDiv.style.width = '100%'; // Займає повну ширину

    // Робимо користувача клікабельним
    userDiv.addEventListener('click', () => {
        console.log('Користувач ' + user.name + ' ' + user.surname + ' був клікнутий');
        // Заповнюємо обводку зеленим коліром при кліку, а при повторному кліку зникає
        userDiv.style.backgroundColor = userDiv.style.backgroundColor === 'green' ? '' : 'green';

        // Зберігаємо вибраного користувача
        if (selectedUsers.includes(userDiv)) {
            selectedUsers = selectedUsers.filter(u => u !== userDiv);
        } else {
            selectedUsers.push(userDiv);
        }
    });

    // Додаємо користувача до контейнера
    userContainer.appendChild(userDiv);
}
});

// Додаємо контейнер до слайдера
slider.appendChild(userContainer);

// Додаємо обробник подій до кнопки "OK"
document.getElementById('okButton').addEventListener('click', async () => {
    if (selectedUsers.length > 0) {
        if (isCreatingChat) return;
        isCreatingChat = true;
        // Запитуємо назву групи або використовуємо ім'я користувача, якщо вибрано тільки одного користувача
        let groupName = selectedUsers.length > 1 ? prompt("Введіть назву для групи") : selectedUsers[0].textContent;
        
        if (groupName === null) {
            // Перевіряємо, чи користувач натиснув кнопку "-"
            
        } else if (existingDialogs.includes(groupName) || groupName in existingGroups) {
            // Перевіряємо, чи вже існує діалог або група з такою назвою
            alert('Діалог або група з такою назвою вже існує!');
           
        } else {



            let chat = null;
            if(selectedUsers.length > 1){
                console.log("Debug:Group");
            let response = await fetch('/create-chat', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ name: groupName,isGroup: true })
            });
            chat = await response.json();
            console.log("Its group "+groupName+"details "+chat);
            }
            else{
                console.log("Debug:Dialog");
                let response = await fetch('/create-chat', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({ name: groupName + '&' + curentusername,isGroup: false})
                });
                chat = await response.json();
                console.log("Its dialog "+groupName + ' & '  + curentusername+"details "+chat.chatId+" ");

            }



         
            
            

            // Створюємо новий контейнер для групи або діалогу
            let groupDiv = document.createElement('div');
            groupDiv.textContent = groupName + (selectedUsers.length > 1 ? ' (' + (selectedUsers.length+1) + ' members)' : '');
            groupDiv.style.border = '1px solid green';
            groupDiv.style.borderRadius = '10px';
            groupDiv.style.margin = '10px';
            groupDiv.style.textAlign = 'center';
            groupDiv.style.padding = '20px';
            groupDiv.style.width = '100%'; // Займає повну ширину

            // Робимо групу або діалог клікабельним
// Робимо групу або діалог клікабельним
groupDiv.addEventListener('click', () => {
    console.log((existingGroups[groupName] && existingGroups[groupName].length > 1 ? 'Група ' : 'Діалог ') + groupName + ' був клікнутий');
    membersname="";
    // Змінюємо назву чату на ім'я користувача або ім'я чату
    document.getElementById('chatTitle2').textContent = groupName;
    alert(groupName);
    if (existingGroups[groupName] && existingGroups[groupName].length > 1) {
        console.log('В групі знаходяться:');
        existingGroups[groupName].forEach(user => {
            console.log(user.textContent);
            
            if (user.textContent !== null) {
                membersname += user.textContent +" |" + "\n";
            }
            
            

        });
        membersname += curentusername +" |" + "\n";
        console.log(curentusername );
        
    }
});



            // Додаємо групу або діалог до іншого контейнера
            document.querySelector('.right .MassangerRBox .sketchy #Massage').appendChild(groupDiv);

            // Зберігаємо назву групи або діалогу
            if (selectedUsers.length > 1) {
                existingGroups[groupName] = [...selectedUsers]; // Зберігаємо копію масиву вибраних користувачів
                let modal = document.getElementById('myModal');
                modal.style.display = "none";
            } else {
                existingDialogs.push(groupName);
                let modal = document.getElementById('myModal');
                modal.style.display = "none";
            }



   // Додавання користувачів до групи або діалогу
   let userIds = [];
   if(selectedUsers.length > 1){
   for (let user of existingGroups[groupName]) {
       let response = await fetch('/get-user-id', {
           method: 'POST',
           headers: {
               'Content-Type': 'application/json'
           },
           body: JSON.stringify({ userName: user.textContent })
       });
       let userId = await response.json();
       userIds.push(userId);
   }
   userIds.push(currentUserId);
   for (let i=0 ; i<userIds.length; i++) {
       console.log("Тут айді юзеріа  :"+i);
       console.log(userIds[i]);
   }
    }else{
        let response = await fetch('/get-user-id', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ userName: groupName })
        });
        let userId = await response.json();
        userIds.push(userId);
        userIds.push(currentUserId);
        for (let i=0 ; i<userIds.length; i++) {
            console.log("Тут айді юзерів  :"+i);
            console.log(userIds[i]);
        }
    }




   for (let i=0 ; i<userIds.length; i++) {
     
       await fetch('/add-user-to-chat', {
           method: 'POST',
           headers: {
               'Content-Type': 'application/json'
           },
           body: JSON.stringify({ chatId: chat.chatId, userId: userIds[i]})
       });
   }




            
   //document.getElementById('okButton').removeEventListener('click', handleOkButtonClick);
            // Очищуємо масив вибраних користувачів
            selectedUsers = [];
             console.log('Debug chatid :',chat.chatId);
             console.log('Debug Userid :',userIds[0]);
             socket.emit('new chat', userIds );
             //Сокет для нового чату
            isCreatingChat = false;
        }
    }
});




  // Відображення модального вікна
  let modal = document.getElementById('myModal');
  modal.style.display = "block";

});


async function getUserName(userId) {
    try {
        const response = await fetch('/get-user-name', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ userId })
        });

        const fullName = await response.json();
        return fullName;
    } catch (error) {
        console.error('Error getting user name:', error);
        throw new Error('Error getting user name');
    }
}

async function getChatUsersId(chatId) {
    try {
        const response = await fetch('/get-chat-users', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ chatId })
        });

        const userIds = await response.json();
        return userIds;
    } catch (error) {
        console.error('Error getting chat users:', error);
        throw new Error('Error getting chat users');
    }
}
//
//
//----------------------------------------------------------------
//----------------------------------------------------------------
//Повідомлення: код для відправки повідомлень
/*
_________________¶¶¶¶¶¶¶¶
_______________¶¶¶¶¶¶¶¶¶¶¶¶¶¶
_________________¶¶¶¶¶¶¶¶¶¶____¶¶¶¶¶¶¶¶¶
_________________¶¶¶¶¶¶¶¶¶__¶¶¶¶¶¶¶¶¶¶¶¶¶¶
_______¶¶¶¶¶¶¶____¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶
____¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶
__¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶___________¶¶¶
¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶______________¶¶
¶¶¶¶¶¶¶¶¶______¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶
___________¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶
_______¶¶¶¶¶¶¶¶¶¶¶¶¶¶¶__¶¶¶¶¶_______¶¶¶¶¶¶¶¶¶¶¶¶¶
______¶¶¶¶¶¶¶¶¶¶¶________¶¶¶¶¶_________¶¶¶¶¶¶¶¶¶¶
______¶¶¶¶¶¶¶¶¶__________¶¶¶¶¶¶_________¶¶¶¶¶¶¶¶¶
______¶¶¶¶¶¶¶¶____________¶¶¶¶¶__________¶¶¶¶¶¶¶¶
______¶¶¶¶¶¶______________¶¶¶¶¶¶__________¶¶¶¶¶¶¶
______¶¶¶¶¶________________¶¶¶¶¶¶__________¶¶¶¶¶¶
_______¶¶¶¶_________________¶¶¶¶¶¶__________¶¶¶¶¶
_______¶¶__________________¶¶¶¶¶¶¶_________¶¶¶¶
_______¶¶___________________¶¶¶¶¶¶¶_________¶¶
_______¶____________________¶¶¶¶¶¶¶_________¶
_____________________________¶¶¶¶¶¶
_____________________________¶¶¶¶¶¶¶
_____________________________¶¶¶¶¶¶¶
___________________________¶¶¶¶¶¶¶¶¶¶
________________________¶¶¶111¶¶¶¶¶¶¶
_________________¶¶¶¶¶¶111111111¶¶¶¶¶¶¶
_______________¶¶1111111111111111¶¶¶¶¶¶¶
_____¶¶¶¶¶1111111111111¶1111¶¶¶¶¶¶¶1111¶¶¶
__¶¶¶¶¶¶1111111111111111111¶¶11¶¶¶¶¶¶¶11¶¶¶¶
¶¶1111111111111111111111110n¶¶¶¶¶¶¶¶¶¶¶¶¶¶
11111111111111111111111111111¶¶¶¶¶¶¶¶¶¶¶¶¶¶ */
//----------------------------------------------------------------
//----------------------------------------------------------------
//
//sendMessageButton

async function createMessage(messageData) {
    try {
      const response = await fetch('/create-message', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(messageData),
      });
  
      const data = await response.json();
      console.log(data); // Інформація про відповідь сервера
      return data; // Повертаємо дані з сервера
    } catch (error) {
      console.error('Error creating message:', error);
      throw new Error('Error creating message');
    }
  }
  // Створюємо об'єкт з даними для створення повідомлення


document.getElementById('sendMessageButton').addEventListener('click', async function() {
    let SenderMessageText = document.getElementById('messageInput').value;
    console.log(SenderMessageText);
    console.log("Контроль чат ід:",currentChatIdForMessage);
    
    const messageData = {
        chatId: currentChatIdForMessage, // Ідентифікатор чату
        userId: currentUserId, // Ідентифікатор користувача
        message: SenderMessageText, // Текст повідомлення
        timestamp: new Date().toISOString() // Додаємо поточну дату та час відправлення у форматі ISO
    };
    
      createMessage(messageData)
  .then(data => {
    // Обробка успішної відповіді від сервера

//DIV
// Створюємо новий div елемент
let newDiv = document.createElement('div');
newDiv.className = 'message receiver';

// Створюємо div для тексту
let textDiv = document.createElement('div');
textDiv.className = 'text';

// Створюємо strong елемент для імені користувача
let strong = document.createElement('strong');
strong.textContent = curentusername; 

// Створюємо p елемент для тексту повідомлення
let p = document.createElement('p');
p.textContent = SenderMessageText;

// Додаємо strong і p до textDiv
textDiv.appendChild(strong);
textDiv.appendChild(p);

// Додаємо textDiv до newDiv
newDiv.appendChild(textDiv);

// Створюємо p елемент для дати та часу відправлення
let timestamp = document.createElement('p');
let now = new Date();
timestamp.textContent = now.toLocaleString(); // Форматуємо дату та час відправлення
timestamp.style.fontSize = '0.8em'; // Зменшуємо розмір шрифту для дати та часу
textDiv.appendChild(timestamp); // Додаємо дату та час до newDiv

// Додаємо новий div до DIALOG div
let dialogDiv = document.getElementById('DIALOG');
dialogDiv.appendChild(newDiv);

//End DIV
//TODo

fetch('/get-user-id-bycid', {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({ chatid: currentChatIdForMessage }) // Поміняйте currentChatIdForMessage на chatid
})
.then(response => {
    if (!response.ok) {
        throw new Error('Network response was not ok');
    }
    return response.json(); // Отримання тіла відповіді у форматі JSON
})
.then(data => {
    const dataArray = Array.isArray(data) ? data : [data];
    console.log('Response from server:', dataArray); // Відображення даних від сервера
    console.log('Response from server:', data,"current chat id",currentChatIdForMessage); // Відображення даних від сервера
    socket.emit('Send Massage', dataArray,currentChatIdForMessage,currentUserId,curentusername,SenderMessageText); 

})
.catch(error => {
    // Обробка помилки
    console.error('Error creating message:', error);
});

   

    console.log('Message created successfully:', data);
  })
  .catch(error => {
    // Обробка помилки
    console.error('Error creating message:', error);
  });

  
    

});

//----------------------------------------------------------------
//Отримання повідомлень з бд
async function getMessages(chatId) {
    try {
      const response = await fetch(`/get-messages/${chatId}`);
      const messages = await response.json();
      return messages;
    } catch (error) {
      console.error('Error fetching messages:', error);
      throw new Error('Error fetching messages');
    }
  }

  function createMessageElementNotifications(message, isSender) {
    // Створюємо новий div для повідомлення
    const messageDiv = document.createElement('div');
    messageDiv.className = isSender ?  'message receiver': 'message sender';
  
    // Додамо вміст повідомлення
    const textDiv = document.createElement('div');
    textDiv.className = 'text';
    const strong = document.createElement('strong');
   
    getUserName(message.userId)
    .then(fullName => {
        console.log('User full name:', fullName);
        strong.textContent=fullName;
        // Далі ви можете використати fullName для подальшої обробки
    })
    .catch(error => {
        console.error('Error:', error);
    });

   
    const p = document.createElement('p');
    p.textContent = message.message; // Текст повідомлення
    textDiv.appendChild(strong);
    textDiv.appendChild(p);
    messageDiv.appendChild(textDiv);
  
    // Додамо час відправлення
    const timestamp = document.createElement('p');
    timestamp.textContent = new Date(message.timestamp).toLocaleString(); // Припустимо, що timestamp містить дату та час відправлення
    textDiv.appendChild(timestamp);
  
    // Додаємо повідомлення до контейнера чату
    const chatContainer = document.getElementById('chatContainer');
    document.getElementById("BellMouseHoverText").appendChild(messageDiv);
  }

//Розміщення дівів 
function createMessageElement(message, isSender) {
    // Створюємо новий div для повідомлення
    const messageDiv = document.createElement('div');
    messageDiv.className = isSender ?  'message receiver': 'message sender';
  
    // Додамо вміст повідомлення
    const textDiv = document.createElement('div');
    textDiv.className = 'text';
    const strong = document.createElement('strong');
   
    getUserName(message.userId)
    .then(fullName => {
        console.log('User full name:', fullName);
        strong.textContent=fullName;
        // Далі ви можете використати fullName для подальшої обробки
    })
    .catch(error => {
        console.error('Error:', error);
    });

   
    const p = document.createElement('p');
    p.textContent = message.message; // Текст повідомлення
    textDiv.appendChild(strong);
    textDiv.appendChild(p);
    messageDiv.appendChild(textDiv);
  
    // Додамо час відправлення
    const timestamp = document.createElement('p');
    timestamp.textContent = new Date(message.timestamp).toLocaleString(); // Припустимо, що timestamp містить дату та час відправлення
    textDiv.appendChild(timestamp);
  
    // Додаємо повідомлення до контейнера чату
    const chatContainer = document.getElementById('chatContainer');
    document.getElementById("DIALOG").appendChild(messageDiv);
  }


//----------------------------------------------------------------
//----------------------------------------------------------------
//Отримання чатів з бази даних
//----------------------------------------------------------------
//----------------------------------------------------------------
// Функція для отримання списку чатів з сервера
//async function fetchChats() {
   // try {
       // let response = await fetch('/get-chats'); // Викликаємо маршрут для отримання чатів
       // let chats = await response.json();
       // return chats;
   // } catch (error) {
      //  console.error('Error fetching chats:', error);
      //  return [];
   // }
//}









/*
░░░░░░░░░░░▄▄▄▄▄▄▄▄▄▄▄▄░░░░░░░░░░░
░░░░░░░▄▄██▀▀▀▀▀▀▀▀▀▀▀▀██▄▄░░░░░░░
░░░░░▄██▀▄░░░░░░░░░░░░░░▄▀██▄░░░░░
░░░▄██▀▄███░░░░░░░░░░░░███▄▀██▄░░░
░░██▀▄██████░░░░░░░░░░██████▄▀██░░
░██░█████████▄░░░░░░▄█████████░██░
██░███████████▄░░░░▄███████████░██
██▄███████████▀▄▄▄▄▀███████████▄██
█████████████░██████░█████████████
██░░░░░░░░░░░░██████░░░░░░░░░░░░██
██░░░░░░░░░░░░░▀▀▀▀░░░░░░░░░░░░░██
▀█░░░░░░░░░░░░██████▄░░░░░░░░░░░██
░██░░░░░░░░░▄████████▄░░░░░░░░░██░
░░██▄░░░░░░▄██████████▄░░░░░░▄██░░
░░░▀██▄░░░▄████████████▄░░░▄██▀░░░
░░░░░▀██▄░▀████████████▀░▄██▀░░░░░
░░░░░░░▀▀██▄▄▄██████▄▄▄██▀▀░░░░░░░
░░░░░░░░░░░▀▀▀▀▀▀▀▀▀▀▀▀░░░░░░░░░░░
*/

// Функція для відображення чатів на сторінці
async function displayChats() {
//let chats = await fetchChats();
//console.log('Chats:',chats); 
document.querySelector('.right .MassangerRBox .sketchy #Massage').innerHTML = "";

let chatIds = [];
//запит на отримання ід чатів користувача    
let chats=[];

await fetch('/get-user-chats', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ currentUserId }),
  })
  .then(response => response.json())
  .then(data => {
   
    chatIds = data;
    console.log('ID чатів:', chatIds);
  })
  .catch((error) => {
    console.error('Помилка:', error);
  });
//Кінець запиту на отримання ід чатів користувача
//----------------------------------------------------------------
//Запит на отримання інформації про чати : 
console.log('Чат ідс',chatIds);
await fetch('/get-chats', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    
    body: JSON.stringify({ chatIds }), 
})
.then(response => response.json())
.then(data => {
    chats = data;
    console.log('Чати:', chats);
  
 

})
.catch((error) => {
  console.error('Помилка:', error);
});
  //Кінець запиту на отримання інформації про чати 
  //----------------------------------------------------------------
  //Додавання чатів на сторінку
  // Очищаємо дів перед додаванням нових чатів
// Очищаємо дів перед додаванням нових чатів
let chatDivv = document.querySelector('.right .MassangerRBox .sketchy #Massage');

        // Дочекатися перерендерингу, перш ніж змінити прозорість
        //await new Promise(resolve => setTimeout(resolve, 3000));


   
  
  for (let chat of chats) {
    console.log('Чат :', chat.name, chat.isGroup, chat._id);
        let groupName ="";
        // Запитуємо назву групи або використовуємо ім'я користувача, якщо вибрано тільки одного користувача
        if (chat.isGroup) {
        groupName = chat.name
        }else{
            let str = chat.name;
            str = str.replace('&', '');
            let newStr =str.replace(`${curentusername}`, '').trim();
            
            
            groupName = newStr;
            console.log('Назва групи :', groupName);
        }
        
        // ID чату, для якого ви хочете отримати користувачів
let currentChatId = chat._id;

await fetch('/get-chat-users', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
  body: JSON.stringify({ currentChatId }),
})
.then(response => response.json())
.then(userIds => {
  
    //console.log('User Names:', userIds);
    selectedUsers =userIds;
  console.log('Вибрані користувачі:',groupName,selectedUsers)

})
.catch((error) => {
  console.error('Error:', error);
});

        // Створюємо новий контейнер для групи або діалогу
        let groupDiv = document.createElement('div');
        groupDiv.textContent = groupName + (selectedUsers.length > 2 ? ' (' + (selectedUsers.length) + ' members)' : '');
        groupDiv.style.border = '1px solid green';
        groupDiv.style.borderRadius = '10px';
        groupDiv.style.margin = '10px';
        groupDiv.style.textAlign = 'center';
        groupDiv.style.padding = '20px';
        groupDiv.style.width = '100%'; // Займає повну ширину

        // Додаємо групу або діалог до іншого контейнера
        document.querySelector('.right .MassangerRBox .sketchy #Massage').appendChild(groupDiv);

        
        if (chat.isGroup) {

            existingGroups[groupName] = [...selectedUsers]; // Зберігаємо копію масиву вибраних користувачів
            console.log("existingGroups: ",existingGroups[groupName]);
        } else {
            existingDialogs.push(groupName);
        }

        
        socket.on('update massages', (dataArray, currentChatIdForMessage2, senderId) => {
            // Отримуємо ідентифікатор поточного користувача (якщо він доступний на сторінці)
           
            // Перевіряємо, чи ми отримали повідомлення від іншого користувача і чи ми ще не отримали це повідомлення
            if ( dataArray.includes(currentUserId)) {
                // Перевіряємо, чи повідомлення призначено для поточного чату
                if (currentUserId !== senderId){

                if (currentChatIdForMessage === currentChatIdForMessage2) {
                   
                        getMessages(currentChatIdForMessage2)
                        .then(messages => {
                          document.getElementById("DIALOG").innerHTML = "";
                          // Обробка отриманих повідомлень
                          console.log('Messages:', messages);
                          messages.forEach(message => {
                              const isSender = message.userId === currentUserId; // Перевірка, чи ви є відправником цього повідомлення
                              createMessageElement(message, isSender); // Створення елементу повідомлення
                            });
                        })
                        .catch(error => {
                          // Обробка помилки
                          console.error('Error fetching messages:', error);
                        });
                    }

                }
            }
        });
        
//Клік 
/*
▒▒▒▒▒▒▒▒▄▄▄▄▄▄▄▄▒▒▒▒▒▒
▒▒█▒▒▒▄██████████▄▒▒▒▒
▒█▐▒▒▒████████████▒▒▒▒
▒▌▐▒▒██▄▀██████▀▄██▒▒▒
▐┼▐▒▒██▄▄▄▄██▄▄▄▄██▒▒▒
▐┼▐▒▒██████████████▒▒▒
▐▄▐████─▀▐▐▀█─█─▌▐██▄▒
▒▒█████──────────▐███▌
▒▒█▀▀██▄█─▄───▐─▄███▀▒
▒▒█▒▒███████▄██████▒▒▒
▒▒▒▒▒██████████████▒▒▒
▒▒▒▒▒█████████▐▌██▌▒▒▒
▒▒▒▒▒▐▀▐▒▌▀█▀▒▐▒█▒▒▒▒▒
▒▒▒▒▒▒▒▒▒▒▒▐▒▒▒▒▌▒▒▒▒▒

*/
        groupDiv.addEventListener('click', () => {

// Додамо клас для анімації
groupDiv.classList.add('clicked');
// Зняття класу після закінчення анімації, можна вказати відповідний таймаут
setTimeout(() => {
    groupDiv.classList.remove('clicked');
}, 1000); // Наприклад, анімація триває 1 секунду

            console.log((existingGroups[groupName] && chat.isGroup ? 'Група ' : 'Діалог ') + groupName + ' був клікнутий');
            membersname="";
            console.log("Дебаг Айді чати при кліку ",chat._id); 
            currentChatIdForMessage = chat._id;
            fetch('/get-chat-users', {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({ currentChatId }),
              })
              .then(response => response.json())
              .then(userIds => {
                
                  //console.log('User Names:', userIds);
                  userIdsForMessage =userIds;
                console.log('Вибрані користувачі:',userIds);
              
              })
            
            //
            //
            //Отримання повідомлень
            //
            //
            getMessages(currentChatIdForMessage)
  .then(messages => {
    document.getElementById("DIALOG").innerHTML = "";
    // Обробка отриманих повідомлень
    console.log('Messages:', messages);
    messages.forEach(message => {
        const isSender = message.userId === currentUserId; // Перевірка, чи ви є відправником цього повідомлення
        createMessageElement(message, isSender); // Створення елементу повідомлення
      });
  })
  .catch(error => {
    // Обробка помилки
    console.error('Error fetching messages:', error);
  });
            // Змінюємо назву чату на ім'я користувача або ім'я чату
            document.getElementById('chatTitle2').textContent = groupName;
            if (existingGroups[groupName] && chat.isGroup) {
                console.log('В групі знаходяться:');
                existingGroups[groupName].forEach(user => {
                    console.log(user);
                    
                    if (user.textContent !== null) {
                        membersname += user +" |" + "\n";
                    }
                    
                    
        
                });

                
            }
        });

        selectedUsers = [];
    };
   




}





// Викликати функцію для оновлення чатів кожні 5 секунд
//setInterval(displayChats, 5000);





//----------------------------------------------------------------
//----------------------------------------------------------------
//СОКЕТ айо
//----------------------------------------------------------------
//----------------------------------------------------------------

socket.on('update chat list', (userIds) => {
    // Оновлюємо список чатів
    if (userIds.includes(currentUserId)){
    displayChats();
    console.log('Debug: Я отримав додавання в чат ');
    }
  
               
  });




