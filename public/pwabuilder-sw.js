const CACHE = "pwabuilder-offline";

const offlineFallbackPage = "index.html";

self.addEventListener("install", function (event) {
    console.log("[PWA Builder] Install Event processing");

    event.waitUntil(
        caches.open(CACHE).then(function (cache) {
            console.log("[PWA Builder] Cached offline page during install");

            if (offlineFallbackPage === "index.html") {
                return cache.add(new Response("Update the value of the offlineFallbackPage constant in the serviceworker."));
            }

            return cache.add(offlineFallbackPage);
        })
    );
});

self.addEventListener("fetch", function (event) {
    if (event.request.method !== "GET") return;

    event.respondWith(
        fetch(event.request)
            .then(function (response) {
            console.error("[PWA Builder] Network request Failed. Serving offline page " + response.url);
            event.waitUntil(updateCache(event.request, response.clone()));
            return response;
        })
        .catch(function (error) {
            console.error("[PWA Builder] Network request Failed. Serving offline page " + error);
            return fromCache(event.request);
        })
    );
});

function fromCache(request) {
    return caches.open(CACHE).then(function (cache) {
        return cache.match(request).then(function (matching) {
            if (!matching || matching.status === 404) {
                return Promise.reject("no-match");
            }

            return matching;
        });
    });
}

function updateCache(request, response) {
    return caches.open(CACHE).then(function (cache) {
      return cache.put(request, response);
    });
  }