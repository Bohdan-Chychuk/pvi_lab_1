
document.getElementById("registerbtn").addEventListener("click", async function(event){
      event.preventDefault(); // Завжди запобігаємо переходу на наступну сторінку
    
      var firstName = document.getElementById("name").value;
      var lastName = document.getElementById("surname").value;
      var password = document.getElementById("psw").value;
      
    
      // Перевірка за допомогою регулярних виразів
      let nameRegex = /^[A-ZА-Я][a-zа-я]{2,30}$/; // Перша буква велика, всі інші маленькі, без символів, максимальна довжина 30
      let cyrillicRegex = /[А-Яа-яЁё]/; // Перевірка на наявність кирилиці
      let latinRegex = /[A-Za-z]/; // Перевірка на наявність латиниці
    
      // Перевірка імені та прізвища
      if (!nameRegex.test(firstName) || !nameRegex.test(lastName) || 
          (cyrillicRegex.test(firstName) !== cyrillicRegex.test(lastName)) || 
          (latinRegex.test(firstName) !== latinRegex.test(lastName))) {
        alert("Ім'я та прізвище повинні починатися з великої літери, містити лише букви, не містити символів, мати довжину 2-30 символів та бути написаними на одній мові");
        return;
      }
    
  
    
     
// Відправка даних на сервер
try {
  let response = await fetch('/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      name: firstName,
      surname: lastName,
      password: password
    })
  });
  if(response.status === 400)
  {
    alert('Користувач з таким ім\'ям та прізвищем не існує або пароль неправильний');
    return ;
  }
  if (!response.ok) {
    throw new Error(`HTTP error! status: ${response.status}`);
  } else {
    let user = await response.json(); // Отримання об'єкта користувача від сервера
    document.cookie = `name=${firstName}; path=/`;
    document.cookie = `surname=${lastName}; path=/`;
    document.cookie = `userId=${user.userId}; path=/`; // Зберігання userId в куках
    alert('Успішний вхід в систему');
    window.location.href = "/students"; // Перехід на наступну сторінку
  }
} catch (error) {
  console.error('Помилка:', error);
  alert('Помилка при вході в систему');
  return ;
}

    });
 