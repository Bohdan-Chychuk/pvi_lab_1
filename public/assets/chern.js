//mongo
const startMongoDB =async()=>{
    try {
      await client.connect();
      console.log('MONGO:Connected to the database');
    }
      catch(error){
        console.log('MONGO:Error connecting to the database');
        console.log(error);
      }
    }
    startMongoDB();
    //
    //Реєстрація нового користувача
    //
    app.post('/add-User', async (req, res) => {
      try {
        let user = req.body;
        if (!user) {
          throw new Error('User object is undefined');
        }
    
        // Перевірка, чи користувач вже існує
        let existingUser = await client.db('PVI').collection('Users').findOne({ name: user.name, surname: user.surname });
        if (existingUser) {
          res.status(400).json({ message: "Користувач з таким ім'ям та прізвищем вже існує" });
          console.log("Користувач з таким ім'ям та прізвищем вже існує");
          return;
        }
    
        // Додавання нового користувача
        let result = await client.db('PVI').collection('Users').insertOne(user);
        res.status(200).json({ message: "User added successfully", userId: result.insertedId }); // Повертаємо userId
        console.log('User added successfully');
      } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error adding user'});
      }
  });
  
    //
    //Вхід користувача
    //
   app.post('/login', async (req, res) => {
      try {
        let user = req.body;
        if (!user) {
          throw new Error('User object is undefined');
        }
    
        // Перевірка, чи користувач існує
        let existingUser = await client.db('PVI').collection('Users').findOne({ name: user.name, surname: user.surname });
        if (!existingUser) {
          res.status(400).json({ message: "Користувач з таким ім'ям та прізвищем не існує" });
          console.log("Користувач з таким ім'ям та прізвищем не існує");
          return;
        }
        // Перевірка пароля
        else if (user.password !== existingUser.password) {
          res.status(400).json({ message: "Неправильний пароль" });
          console.log("Неправильний пароль");
          return;
        }
        else{
        res.status(200).json({ message: "Успішний вхід в систему", userId: existingUser._id }); // Повертаємо userId
        console.log('Успішний вхід в систему');
        }
      } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error logging in user'});
      }
  });
  
    
  
    // Маршрут для створення нового чату
  app.post('/create-chat', async (req, res) => {
    try {
        let chat = req.body;
        if (!chat) {
            throw new Error('Chat object is undefined');
        }
  
        // Додавання нового чату
        let result = await client.db('PVI').collection('Chats').insertOne(chat);
        res.status(200).json({ message: "Chat created successfully", chatId: result.insertedId });
        console.log('Chat created successfully');
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error creating chat'});
    }
  });
  
  // Маршрут для додавання користувача до чату
  app.post('/add-user-to-chat', async (req, res) => {
    try {
        let { chatId, userId } = req.body;
        if (!chatId || !userId) {
            throw new Error('Chat ID or User ID is undefined');
        }
  
        // Додавання користувача до чату
        await client.db('PVI').collection('UserChats').insertOne({ chatId, userId });
        res.status(200).json({ message: "User added to chat successfully"});
        console.log('User added to chat successfully');
    } catch (error) {
        console.log(error);
        res.status(500).json({ message: 'Error adding user to chat'});
    }
  });
  
  
  
    
  //mongo 
  app.get('/get-Users', async (req, res) => {
    try {
      let users = await client.db('PVI').collection('Users').find().toArray();
      res.status(200).json(users);
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: 'Error getting users'});
    }
  });